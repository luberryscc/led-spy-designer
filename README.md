# LED Spy Designer

The LED Spy designer is a feature rich configuration software for my [LED Spy](https://gitlab.com/Luberry/smashboxledspy) boards.

# Support This Project

If you would like to support this project, consider purchasing my LED Spy Board from my [store](https://www.luberryscc.com/product/led-spy-board). Available Soon™.
Or even buying me a beer. [LCC Paypal](paypal.me/LuberrysCC)

# Features

-   Reactive lighting and per button configuration
    ![LED_Spy_Designer_8](/uploads/7b9d604a3c634e3e0e70d014e293b67d/LED_Spy_Designer_8.png)
-   Breathing effects
-   Brightness adjustment
-   Multiple Profiles, and the ability to map them to the profile switch (Smash Box only)
-   Save and load your controller configuration to share with friends
-   Built in configuration for two types of controller
    -   Smash Box
        ![LED_Spy_Designer_4](/uploads/25665888b0efb04c5d2eeca98e1d0a9a/LED_Spy_Designer_4.png)
    -   Hit Box
        ![LED_Spy_Designer_6](/uploads/915bd04002e7d38e72a4b4a34e0aa326/LED_Spy_Designer_6.png)
-   Have a non standard wiring scheme or want to omit some buttons? Enable Mapping Mode and enable/disable buttons, or change their order
    ![LED_Spy_Designer_7](/uploads/b63512f4ff5dc2c2b6af1cbbe8783fc3/LED_Spy_Designer_7.png)
-   Built in firmware flash tool
-   Upload profiles directly to your controller
-   Load configuration values from your controller
-   Cross Platform, Works on Windows, Mac, and Linux
