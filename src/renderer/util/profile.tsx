import * as fs from 'fs';
export enum ProfileType {
    Static = 0,
    Reactive
}

export const BreathInterval = {
    Min: 0.0001, //slowest
    Max: 0.009, //fastest
    Default: 0.002 //sane default
};

export const ProfileTypeSelect = [
    { label: 'Static', value: ProfileType.Static },
    { label: 'Reactive', value: ProfileType.Reactive }
];

export function colorToHex(color: string) {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(color);
    let out = result
        ? new Uint8Array([
              parseInt(result[1], 16),
              parseInt(result[2], 16),
              parseInt(result[3], 16)
          ])
        : new Uint8Array();
    return out;
}
export function colorFromHex(i: number, arr: Buffer) {
    var toHex = (n: number) => {
        var s = n.toString(16);
        return n < 10 ? '0' + s : s;
    };
    if (arr.length < i + 3) {
        throw TypeError(
            'not a valid binary encoded float: too short at index ' + i + ': length ' + arr.length
        );
    }
    return '#' + toHex(arr[i]) + toHex(arr[i + 1]) + toHex(arr[i + 2]);
}

export class ButtonCfg {
    color: string = '';
    colorPressed: string = '';
    constructor(color: string = '', colorPressed: string = '#ffffff') {
        this.color = color;
        this.colorPressed = colorPressed;
    }
}

export enum SmashBoxButtonId {
    AL = 0,
    AD,
    AR,
    AU,
    P1,
    P2,
    P3,
    P4,
    K1,
    K2,
    K3,
    K4,
    CU,
    CR,
    CL,
    CD,
    CM,
    M1,
    M2,
    M3,
    M4,
    M5,
    ST
}

export const get_button_label = (isSmashBox: boolean, btn: SmashBoxButtonId) => {
    switch (btn) {
        case SmashBoxButtonId.AL:
            return 'Left Stick Left';
        case SmashBoxButtonId.AD:
            return 'Left Stick Down';
        case SmashBoxButtonId.AR:
            return 'Left Stick Right';
        case SmashBoxButtonId.AU:
            return 'Left Stick Up';
        case SmashBoxButtonId.P1:
            return 'P1';
        case SmashBoxButtonId.P2:
            return 'P2';
        case SmashBoxButtonId.P3:
            return 'P3';
        case SmashBoxButtonId.P4:
            return 'P4';
        case SmashBoxButtonId.K1:
            return 'K1';
        case SmashBoxButtonId.K2:
            return 'K2';
        case SmashBoxButtonId.K3:
            return 'K3';
        case SmashBoxButtonId.K4:
            return 'K4';
        case SmashBoxButtonId.P1:
            return 'P1';
        case SmashBoxButtonId.P2:
            return 'P2';
        case SmashBoxButtonId.P3:
            return 'P3';
        case SmashBoxButtonId.P4:
            return 'P4';
        case SmashBoxButtonId.CL:
            return 'Right Stick Left';
        case SmashBoxButtonId.CD:
            return 'Right Stick Down';
        case SmashBoxButtonId.CR:
            return 'Right Stick Right';
        case SmashBoxButtonId.CU:
            return 'Right Stick Up';
        case SmashBoxButtonId.CM:
            return 'Extra';
        case SmashBoxButtonId.M1:
            return isSmashBox ? 'Tilt' : 'Share';
        case SmashBoxButtonId.M2:
            return isSmashBox ? 'DPAD Left' : 'Touch Pad';
        case SmashBoxButtonId.M3:
            return isSmashBox ? 'DPAD Up' : 'Home';
        case SmashBoxButtonId.M4:
            return isSmashBox ? 'DPAD Down' : 'L3';
        case SmashBoxButtonId.M5:
            return isSmashBox ? 'DPAD Right' : 'R3';
        case SmashBoxButtonId.ST:
            return 'Start';
    }
};

const floatLong = 1000000;
function encodeFloat(float: number): Uint8Array {
    var num = Math.trunc(float * floatLong);
    return new Uint8Array([
        num & 0x000000ff,
        (num & 0x0000ff00) >> 8,
        (num & 0x00ff0000) >> 16,
        (num & 0xff000000) >> 24
    ]);
}
function decodeFloat(i: number, arr: Buffer): { f: number; i: number } {
    if (arr.length < i + 4) {
        throw TypeError(
            'not a valid binary encoded float: too short at index ' + i + ': length ' + arr.length
        );
    }
    return {
        f: ((arr[i + 3] << 24) | (arr[i + 2] << 16) | (arr[i + 1] << 8) | arr[i]) / floatLong,
        i: i + 4
    };
}
function encodeUint16(num: number): Uint8Array {
    return new Uint8Array([num & 0x00ff, (num & 0xff00) >> 8]);
}
function decodeUint16(i: number, arr: Buffer): { u: number; i: number } {
    if (arr.length < i + 2) {
        throw TypeError(
            'not a valid binary encoded uint16: too short at index ' + i + ': length ' + arr.length
        );
    }
    return { u: (arr[i + 1] << 8) | arr[i], i: i + 2 };
}

export const defaultColorsSB = [
    [
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffff00', colorReact: '#ffffff' },
        { color: '#ffff00', colorReact: '#ffffff' },
        { color: '#ffff00', colorReact: '#ffffff' },
        { color: '#ffff00', colorReact: '#ffffff' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' }
    ],
    [
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffff00', colorReact: '#ffffff' },
        { color: '#ffff00', colorReact: '#ffffff' },
        { color: '#ffff00', colorReact: '#ffffff' },
        { color: '#ffff00', colorReact: '#ffffff' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' }
    ],
    [
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ffffff', colorReact: '#ff0000' }
    ]
];
export const defaultColorsHB = [
    [
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ff0000', colorReact: '#ffffff' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' },
        { color: '#ffffff', colorReact: '#ff0000' }
    ]
];

const colorChoose = (isSmashBox: boolean) => {
    return isSmashBox ? defaultColorsSB : defaultColorsHB;
};

export class Profile {
    pType: ProfileType = ProfileType.Static;
    breathInterval: number = 0;
    buttons: ButtonCfg[] = [];
    constructor(profileId: number = 0, isSmashBox: boolean = true) {
        this.setDefaults(profileId, isSmashBox);
    }

    setDefaults(profileId: number = 0, isSmashBox: boolean = true) {
        const colorType = colorChoose(isSmashBox);
        const id = profileId >= colorType.length ? 0 : profileId;
        const colors = colorType[id];
        var i;
        for (i = 0; i < 23; i++) {
            var btn = new ButtonCfg();
            (btn.color = colors[i].color),
                (btn.colorPressed = colors[i].colorReact),
                this.buttons.push(btn);
        }
        if (profileId == 2) {
            this.pType = ProfileType.Reactive;
        }
    }
}
export class Mapping {
    value: number = 0;
    disabled: boolean = false;
}
const default_mappings_sb = [
    { value: 20, disabled: false },
    { value: 19, disabled: false },
    { value: 17, disabled: false },
    { value: 18, disabled: false },

    { value: 10, disabled: false },
    { value: 9, disabled: false },
    { value: 8, disabled: false },
    { value: 7, disabled: false },
    { value: 6, disabled: false },
    { value: 5, disabled: false },
    { value: 4, disabled: false },
    { value: 3, disabled: false },

    { value: 11, disabled: false },
    { value: 2, disabled: false },
    { value: 12, disabled: false },
    { value: 1, disabled: false },
    { value: 0, disabled: false },

    { value: 21, disabled: false },
    { value: 15, disabled: false },
    { value: 16, disabled: false },
    { value: 14, disabled: false },
    { value: 13, disabled: false },

    { value: 22, disabled: false }
];
const default_mappings_hb = [
    { value: 0, disabled: false },
    { value: 1, disabled: false },
    { value: 2, disabled: false },
    { value: 3, disabled: false },

    { value: 5, disabled: false },
    { value: 7, disabled: false },
    { value: 9, disabled: false },
    { value: 11, disabled: false },
    { value: 10, disabled: false },
    { value: 8, disabled: false },
    { value: 6, disabled: false },
    { value: 4, disabled: false },

    { value: 19, disabled: true },
    { value: 18, disabled: true },
    { value: 20, disabled: true },
    { value: 21, disabled: true },
    { value: 22, disabled: true },

    { value: 14, disabled: false },
    { value: 12, disabled: false },
    { value: 13, disabled: false },
    { value: 16, disabled: true },
    { value: 17, disabled: true },

    { value: 15, disabled: false }
];
export class Config {
    version: number = 1;
    profiles: Profile[] = [];
    brightness: number = 0.2;
    maxBrightness: number = 1;
    switchProfile0: number = 0;
    switchProfile1: number = 1;
    switchProfile2: number = 2;
    isSmashBox: boolean = true;
    mappings: Mapping[] = [];
    setDefaults() {
        this.profiles.push(new Profile(0, this.isSmashBox));
        this.profiles.push(new Profile(1, this.isSmashBox));
        this.profiles.push(new Profile(2, this.isSmashBox));
    }
    constructor(isSmashBox: boolean = true) {
        this.mappings = isSmashBox ? default_mappings_sb : default_mappings_hb;
        this.isSmashBox = isSmashBox;
        this.setDefaults();
    }
}

function encodeButton(btn: ButtonCfg, ptype: ProfileType, disabled: boolean) {
    if (disabled) {
        return colorToHex('#000000');
    }
    switch (ptype) {
        case ProfileType.Static: {
            return colorToHex(btn.color);
        }
        case ProfileType.Reactive: {
            return colorToHex(btn.colorPressed);
        }
    }
}
function decodeButton(i: number, arr: Buffer, btn: ButtonCfg, ptype: ProfileType) {
    switch (ptype) {
        case ProfileType.Static:
            {
                btn.color = colorFromHex(i, arr);
                i += 3;
            }
            break;
        case ProfileType.Reactive:
            {
                btn.colorPressed = colorFromHex(i, arr);
                i += 3;
            }
            break;
        default:
            throw TypeError('invalid profile type: ' + ptype);
    }
    return i;
}

function encodeProfile(prof: Profile, mappings: Mapping[]) {
    var arr = new Uint8Array([prof.pType]);
    arr = new Uint8Array([...arr, ...encodeFloat(prof.breathInterval)]);
    prof.buttons.forEach((btn: ButtonCfg, id: number) => {
        arr = new Uint8Array([
            ...arr,
            ...encodeButton(btn, ProfileType.Static, mappings[id].disabled)
        ]);
    });
    if (prof.pType == ProfileType.Reactive) {
        prof.buttons.forEach((btn: ButtonCfg, id: number) => {
            arr = new Uint8Array([...arr, ...encodeButton(btn, prof.pType, mappings[id].disabled)]);
        });
    }
    return arr;
}
function decodeProfile(
    i: number,
    id: number,
    isSmashBox: boolean,
    arr: Buffer
): { prof: Profile; i: number } {
    var prof = new Profile(id, isSmashBox);
    switch (arr[i]) {
        case ProfileType.Static:
            prof.pType = ProfileType.Static;
            break;
        case ProfileType.Reactive:
            prof.pType = ProfileType.Reactive;
            break;
        default:
            throw TypeError('invalid profile type at index ' + i + ':' + arr[i]);
    }
    i++;

    var br = decodeFloat(i, arr);
    i = br.i;
    prof.breathInterval = br.f;

    prof.buttons.forEach((btn: ButtonCfg) => {
        i = decodeButton(i, arr, btn, ProfileType.Static);
    });
    if (prof.pType == ProfileType.Reactive) {
        prof.buttons.forEach((btn: ButtonCfg) => {
            i = decodeButton(i, arr, btn, ProfileType.Reactive);
        });
    }

    return { prof: prof, i: i };
}
function disableMappings(prof: Profile, mappings: Mapping[]) {
    prof.buttons.forEach((btn: ButtonCfg, id: number) => {
        mappings[id].disabled =
            btn.color === '#000000' &&
            (prof.pType === ProfileType.Static || btn.colorPressed === '#000000');
    });
}

function encodeMappings(mappings: Mapping[]) {
    var arr = new Uint8Array();
    mappings.forEach((m: Mapping) => {
        arr = new Uint8Array([...arr, m.value]);
    });
    return arr;
}
function decodeMappings(i: number, arr: Buffer): { m: Mapping[]; i: number } {
    if (arr.length < i + 23) {
        throw TypeError('invalid byte buffer for mappings must be 23 length: got ' + arr.length);
    }
    var mappings: Mapping[] = [];

    const end = i + 23;
    for (; i < end; i++) {
        var m = new Mapping();
        m.value = arr[i];
        mappings.push(m);
    }
    return { m: mappings, i: i };
}
export function saveConfigToFile(
    cfg: Config,
    file: string,
    errored: (err: Error) => any = (err: Error) => {
        throw err;
    },
    success: () => any = () => {
        console.log('success');
    }
) {
    const cb = (err: Error | null) => {
        if (err) {
            errored(err);
        }
        success();
    };
    const jsonCfg = JSON.stringify(cfg, null, 2);
    fs.writeFile(file, jsonCfg, 'utf-8', cb);
}
export function readConfigFromFile(
    file: string,
    errored: (err: Error) => any = (err: Error) => {
        throw err;
    },
    success: (_cfg: Config) => any = (_cfg: Config) => {
        console.log('success');
    }
) {
    const cb = (err: Error | null, data: string) => {
        if (err) {
            errored(err);
        }

        const jsnData = JSON.parse(data);
        let cfg: Config = {
            ...jsnData
        };
        success(cfg);
    };
    fs.readFile(file, 'utf-8', cb);
}
export function encode(cfg: Config) {
    var arr = new Uint8Array([
        ...encodeUint16(cfg.version),
        ...encodeMappings(cfg.mappings),
        ...encodeFloat(cfg.brightness),
        ...encodeFloat(cfg.maxBrightness)
    ]);

    if (cfg.isSmashBox) {
        arr = new Uint8Array([...arr, cfg.switchProfile0, cfg.switchProfile1, cfg.switchProfile2]);
    }

    //TODO error if len of profiles is too big for uint8
    arr = new Uint8Array([...arr, cfg.profiles.length]);
    cfg.profiles.forEach((prof: Profile) => {
        arr = new Uint8Array([...arr, ...encodeProfile(prof, cfg.mappings)]);
    });

    return arr;
}
export function decode(arr: Buffer, isSmashBox: boolean): Config {
    var cfg = new Config(isSmashBox);
    // skirt around some sort of response before the data
    var i = 7;
    var ur = decodeUint16(i, arr);
    var i = ur.i;
    cfg.version = ur.u;
    var mr = decodeMappings(i, arr);
    i = mr.i;
    cfg.mappings = mr.m;
    var fr = decodeFloat(i, arr);
    i = fr.i;
    cfg.brightness = fr.f;

    var fr2 = decodeFloat(i, arr);
    i = fr2.i;
    cfg.maxBrightness = fr2.f;

    if (cfg.isSmashBox) {
        cfg.switchProfile0 = arr[i];
        cfg.switchProfile1 = arr[i + 1];
        cfg.switchProfile2 = arr[i + 2];
        i += 3;
    }

    var len = arr[i];
    i++;
    var id: number;
    cfg.profiles = [];

    for (id = 0; id < len; id++) {
        var pr = decodeProfile(i, id, isSmashBox, arr);
        i = pr.i;
        cfg.profiles.push(pr.prof);
    }
    disableMappings(cfg.profiles[0], cfg.mappings);

    return cfg;
}
