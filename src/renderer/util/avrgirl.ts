var stk500v2 = require('avrgirl-stk500v2');
var chips = require('electron').remote.require('avrgirl-chips-json');
const SerialPort = require('electron').remote.require('serialport');
export const MEMTYPE_EEPROM = 'eeprom';
export const MEMTYPE_FLASH = 'flash';
var Avrgrl = require('electron').remote.require('avrgirl-arduino');
export type MemType = 'eeprom' | 'flash';
export const EEPROM_SIZE = 4096;
export class Avrgirl {
    private port: string;

    constructor(port: string) {
        this.port = port;
    }

    async read(
        bytes: number,
        memType: MemType,
        onReadSuccess: (response: Buffer) => any,
        onReadError: (err: Error) => any
    ) {
        var stk = this.getStk();
        var callback = (err: Error, data: any) => {
            if (err !== null) {
                console.log('closing serial programmer');
                stk.close();
                onReadError(err);
            } else {
                console.log('closing serial programmer');
                stk.close();
                onReadSuccess(data);
            }
        };
        switch (memType) {
            case MEMTYPE_EEPROM:
                console.log('Reading EEPROM');
                stk.enterProgrammingMode((err: Error) => {
                    if (err !== null) {
                        callback(err, null);
                        return;
                    }

                    stk.readEeprom(bytes, (err: Error, data: Buffer) => {
                        console.log(data);

                        if (err !== null) {
                            callback(err, null);
                            return;
                        }

                        stk.exitProgrammingMode((err: Error) => {
                            callback(err, data);
                        });
                    });
                });
                break;
            case MEMTYPE_FLASH:
                console.log('Reading Flash');
                stk.readFlash(bytes, callback);
                break;
            default:
                onReadError(TypeError(memType));
                break;
        }
    }
    private getStk() {
        var sp = new SerialPort(this.port, {
            baudRate: 115200,
            parser: SerialPort.parsers.raw,
            autoOpen: false
        });

        var options = {
            comm: sp,
            chip: chips.atmega2560,
            frameless: false
        };
        return new stk500v2(options);
    }

    write(
        memType: MemType,
        hex: string | Buffer,
        onWriteSuccess: () => any,
        onWriteError: (error: Error) => any
    ) {
        console.log('here');
        var callback = (e: Error) => {
            if (e !== null) {
                if (stk) {
                    stk.close();
                }
                onWriteError(e);
            } else {
                if (stk) {
                    stk.close();
                }
                onWriteSuccess();
            }
        };
        console.log(stk);
        switch (memType) {
            case MEMTYPE_EEPROM:
                var stk = this.getStk();
                console.log('Flashing EEPROM');
                stk.quickEeprom(hex, (e: Error) => {
                    callback(e);
                });
                break;
            case MEMTYPE_FLASH:
                var avrgirl = new Avrgrl({
                    board: 'mega',
                    port: this.port
                });

                avrgirl.flash(hex, callback);
                break;
            default:
                onWriteError(TypeError(memType));
                break;
        }
    }
}
