import { Avrgirl, MemType } from './avrgirl';

const SerialPort = require('electron').remote.require('serialport');
export type PortList = { label: string; value: string }[];
const skip: any = {
    '/dev/tty.SOC': true,
    '/dev/tty.MALS': true,
    '/dev/tty.Bluetooth-Incoming-Port': true
};
export class Programmer {
    private ports: PortList = [];

    async refreshPorts(onRecievePorts: (ports: PortList) => any) {
        this.ports = await SerialPort.list().then((ports: any[]) => {
            var ret: any[] = [];
            ports.forEach((port: { path: string }) => {
                if (!skip[port.path]) {
                    ret.push({ label: port.path, value: port.path });
                }
            });
            console.log(ret);
            return ret;
        });
        onRecievePorts(this.ports);
    }
    getPorts(): PortList {
        return this.ports;
    }
    async read(
        port: string,
        bytes: number,
        flashType: MemType,
        onReadSuccess: (data: Buffer) => any,
        onReadError: (error: Error) => any
    ) {
        var programmer = new Avrgirl(port);
        programmer.read(bytes, flashType, onReadSuccess, onReadError);
    }

    async write(
        port: string,
        flashType: MemType,
        hex: string | Buffer,
        onWriteSuccess: () => any,
        onWriteError: (error: Error) => any
    ) {
        var programmer = new Avrgirl(port);
        programmer.write(flashType, hex, onWriteSuccess, onWriteError);
    }
}
