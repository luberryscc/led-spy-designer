import * as React from 'react';

import { Input, Select, Layout, Button, notification, Modal, Upload, message } from 'antd';
import {
    ProfileType,
    Config as Cfg,
    ButtonCfg as BCfg,
    encode,
    decode,
    Mapping,
    saveConfigToFile
} from '../util/profile';
import { Programmer as Prog, PortList } from '../util/programmer';
import Profile, { ButtonInfo, colorDisabled } from './Profile';
var fs = require('fs');
import Config from './Config';
import { MEMTYPE_FLASH, MEMTYPE_EEPROM, MemType, EEPROM_SIZE } from '../util/avrgirl';

import { UploadOutlined } from '@ant-design/icons';
export class ProfileInfo {
    id: number = -1;
    isSmashBox: boolean = false;
    buttons: ButtonInfo[] = [];
    breathInterval: number = 0;
    defaultProfileType: ProfileType = ProfileType.Static;
}

export interface Props {}
const { Header, Content } = Layout;
const { Option } = Select;
const version = '0.2.1-alpha'; //TODO stop hardcoding this

export interface State {
    readonly port: string;
    readonly programmer: Prog;
    readonly isFetchingPorts: boolean;
    readonly isProgramming: boolean;
    readonly isReadingConfig: boolean;
    readonly currentConfig: Cfg;
    readonly currentProfileInfo: ProfileInfo;
    readonly showMapping: boolean;
    readonly showFirmwareModal: boolean;
    readonly firmwareModalEnableOk: boolean;
    readonly firmwareModalFile: string;
    readonly showLoadModal: boolean;
}
class Programmer extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);
        let programmer = new Prog();
        let cfg = new Cfg();
        let prof = cfg.profiles[0];
        let info: ProfileInfo = {
            id: 0,
            isSmashBox: cfg.isSmashBox,
            defaultProfileType: prof.pType,
            breathInterval: prof.breathInterval,
            buttons: []
        };

        prof.buttons.forEach((btn: BCfg, bid: number) => {
            const mp = cfg.mappings[bid];
            let binfo = {
                id: bid,
                onChange: (color: string, colorReact: string, all: boolean = false) => {
                    if (all) {
                        this.setAllButtonColors(0, color, colorReact);
                    } else {
                        this.setButton(0, bid, color, colorReact);
                    }
                },
                onMappingChange: (mapping: Mapping) => {
                    this.setMapping(bid, mapping);
                },
                color: mp.disabled ? colorDisabled : btn.color,
                colorReact: mp.disabled ? colorDisabled : btn.colorPressed,
                mapping: mp
            };

            info.buttons.push(binfo);
        });
        this.state = {
            programmer: programmer,
            port: '',
            isFetchingPorts: false,
            isProgramming: false,
            currentConfig: cfg,
            currentProfileInfo: info,
            firmwareModalEnableOk: false,
            showFirmwareModal: false,
            showMapping: false,
            firmwareModalFile: '',
            showLoadModal: false,
            isReadingConfig: false
        };
    }
    setAllButtonColors(pid: number, color: string, colorReact: string) {
        const len = this.state.currentConfig.profiles[pid].buttons.length;
        var i;
        for (i = 0; i < len; i++) {
            this.setButton(pid, i, color, colorReact);
        }
    }
    setError = (error: Error) => {
        notification['error']({
            message: error.name,
            description: error.message
        });
    };
    setMapping = (bid: number, mapping: Mapping) => {
        let cfg = this.getCurrentConfig();
        const cm = cfg.mappings[bid];
        let mappings: Mapping[] = [];
        const disabled = !cm.disabled && mapping.disabled;
        cfg.mappings.forEach((m: Mapping, id: number) => {
            if (disabled) {
                if (id == bid) {
                    mapping.value = cfg.mappings.length - 1;
                    mappings.push(mapping);
                    return;
                }
                if (m.value >= cm.value) {
                    m.value--;
                }
            } else {
                if (id != bid && m.value == mapping.value) {
                    m.value = cm.value;
                } else if (id == bid) {
                    mappings.push(mapping);
                    return;
                }
            }
            mappings.push(m);
        });
        cfg.mappings = mappings;
        this.setCurrentConfig(cfg);
    };
    setButton = (pid: number, bid: number, color: string, colorReact: string) => {
        let cfg = this.getCurrentConfig();
        cfg.profiles[pid].buttons[bid].color = color;
        cfg.profiles[pid].buttons[bid].colorPressed = colorReact;
        console.log(
            'update color for profile (' +
                pid +
                ') and button (' +
                bid +
                '): colors[' +
                color +
                ',' +
                colorReact +
                ']'
        );
        this.setCurrentConfig(cfg);
    };
    tailLayout = {
        wrapperCol: { offset: 8, span: 16 }
    };

    onFinishFailed = (errorInfo: any) => {
        console.log('Failed:', errorInfo);
    };
    getCurrentProfileInfo = () => {
        return this.state.currentProfileInfo;
    };
    isSmashBox = () => {
        return this.getCurrentConfig().isSmashBox;
    };
    getCurrentConfig = () => {
        return this.state.currentConfig;
    };
    onBreathIntervalChange = (interval: number) => {
        let cfg = this.getCurrentConfig();
        let info = this.getCurrentProfileInfo();
        info.breathInterval = interval;
        cfg.profiles[info.id].breathInterval = interval;
        this.setState({ currentConfig: cfg, currentProfileInfo: info });
    };
    setCurrentConfig = (newCfg: any) => {
        let mergedCfg: Cfg = { ...this.getCurrentConfig(), ...newCfg };
        let info = this.getCurrentProfileInfo();
        let pid = info.id;
        if (pid > mergedCfg.profiles.length || pid < 0) {
            pid = 0;
        }
        this.setState({ currentConfig: mergedCfg });
        this.setCurrentProfile(pid);
    };
    onBrightnessChange = (bright: number) => {
        this.setCurrentConfig({ brightness: bright });
    };
    onMaxBrightnessChange = (maxBright: number) => {
        this.setCurrentConfig({ maxBrightness: maxBright });
    };
    onSwitchProfileChange = (prof1: number, prof2: number, prof3: number) => {
        if (this.isSmashBox()) {
            this.setCurrentConfig({
                switchProfile0: prof1,
                switchProfile1: prof2,
                switchProfile2: prof3
            });
        }
    };
    saveConfig = (file: string, errored: (err: Error) => any, success: () => any) => {
        const cfg = this.getCurrentConfig();
        saveConfigToFile(cfg, file, errored, success);
    };
    onProfileTypeChange = (isSmashBox: boolean) => {
        let cfg = new Cfg(isSmashBox);
        let prof = cfg.profiles[0];
        let info = this.getCurrentProfileInfo();

        info.id = 0;
        info.isSmashBox = cfg.isSmashBox;
        info.defaultProfileType = prof.pType;
        info.breathInterval = prof.breathInterval;
        info.buttons = [];
        prof.buttons.forEach((btn: BCfg, bid: number) => {
            const mp = cfg.mappings[bid];
            let binfo = {
                id: bid,
                onChange: (color: string, colorReact: string, all: boolean = false) => {
                    if (all) {
                        this.setAllButtonColors(0, color, colorReact);
                    } else {
                        this.setButton(0, bid, color, colorReact);
                    }
                },
                onMappingChange: (mapping: Mapping) => {
                    this.setMapping(bid, mapping);
                },
                color: mp.disabled ? colorDisabled : btn.color,
                colorReact: mp.disabled ? colorDisabled : btn.colorPressed,
                mapping: mp
            };

            info.buttons.push(binfo);
        });
        this.setState({ currentConfig: cfg, currentProfileInfo: info });
    };
    onProfileChange = (profileId: number) => {
        this.setCurrentProfile(profileId);
    };
    getPorts = () => {
        return this.state.programmer.getPorts();
    };
    refreshPorts = () => {
        this.setState({ isFetchingPorts: true });
        let port = this.state.port;
        this.state.programmer.refreshPorts((ports: PortList) => {
            if (ports.length > 0 && !ports.includes({ label: port, value: port })) {
                this.setState({ port: ports[0].value, isFetchingPorts: false });
            }
        });
    };
    setPort = (port: string) => {
        this.setState({ port: port });
    };
    setCurrentProfileType = (pType: ProfileType) => {
        let cfg = this.getCurrentConfig();
        let info = this.getCurrentProfileInfo();
        const ctype = this.state.currentConfig.profiles[info.id].pType;
        if (pType == ctype) {
            return;
        }
        cfg.profiles[info.id].pType = pType;
        info.defaultProfileType = pType;
        this.setState({ currentConfig: cfg, currentProfileInfo: info });
    };
    programmerError = (error: any) => {
        this.setError(error);
        this.setState({
            isProgramming: false,
            showFirmwareModal: false,
            showLoadModal: false,
            isReadingConfig: false
        });
    };
    programmerSuccess = (msg: string) => {
        return () => {
            this.setState({
                isProgramming: false,
                showFirmwareModal: false,
                showLoadModal: false,
                isReadingConfig: false
            });
            console.log('success');
            message.success(msg);
        };
    };

    programmerWrite = (port: string, flashType: MemType, hex: string | Buffer, msg: string) => {
        this.setState({ isProgramming: true });
        this.state.programmer.write(
            port,
            flashType,
            hex,
            this.programmerSuccess(msg),
            this.programmerError
        );
    };
    flashFirmware = (value: any) => {
        let state = this.state;
        var readFile: string = '';
        try {
            readFile = fs.readFileSync(value, { encoding: 'utf8' });
        } catch (e) {
            if (e.code === 'ENOENT') {
                this.programmerError(
                    Error('could not write flash: please supply a valid path to a hex file.')
                );
            } else {
                return this.programmerError(e);
            }
        }
        this.programmerWrite(
            state.port,
            MEMTYPE_FLASH,
            Buffer.from(readFile),
            'Successfully wrote firmware to controller!'
        );
    };
    writeConfig = () => {
        let state = this.state;

        this.programmerWrite(
            state.port,
            MEMTYPE_EEPROM,
            Buffer.from(encode(state.currentConfig)),
            'Successfully wrote config to controller!'
        );
    };
    configFromBuffer = (hex: Buffer) => {
        const isSmashBox = this.state.currentConfig.isSmashBox;
        try {
            var newCfg = decode(hex, isSmashBox);
            this.setCurrentConfig(newCfg);
            this.programmerSuccess('Succesfully loaded config from controller!')();
        } catch (e) {
            this.programmerError(e);
            return;
        }
    };
    readConfig = () => {
        let state = this.state;
        this.setState({ isReadingConfig: true });
        state.programmer.read(
            state.port,
            EEPROM_SIZE,
            MEMTYPE_EEPROM,
            this.configFromBuffer,
            this.programmerError
        );
    };
    setCurrentProfile = (id: number) => {
        let cfg = this.getCurrentConfig();
        let prof = cfg.profiles[id];
        let info: ProfileInfo = {
            id: id,
            isSmashBox: cfg.isSmashBox,
            defaultProfileType: prof.pType,
            breathInterval: prof.breathInterval,
            buttons: []
        };
        prof.buttons.forEach((btn: BCfg, bid: number) => {
            const mp = cfg.mappings[bid];
            let binfo = {
                id: bid,
                onChange: (color: string, colorReact: string, all: boolean = false) => {
                    if (all) {
                        this.setAllButtonColors(id, color, colorReact);
                    } else {
                        this.setButton(id, bid, color, colorReact);
                    }
                },
                onMappingChange: (mapping: Mapping) => {
                    this.setMapping(bid, mapping);
                },
                color: mp.disabled ? colorDisabled : btn.color,
                colorReact: mp.disabled ? colorDisabled : btn.colorPressed,
                mapping: mp
            };

            info.buttons.push(binfo);
        });
        this.setState({ currentProfileInfo: info });
    };
    showFirmwareModal = () => {
        this.setState({ showFirmwareModal: true });
    };
    handleFirmwareOk = (_e: any) => {
        this.flashFirmware(this.state.firmwareModalFile);
    };

    handleFirmwareCancel = (_e: any) => {
        this.setState({ showFirmwareModal: false });
    };
    onDropdownVisibleChange = (visible: boolean) => {
        if (!visible) {
            return;
        }
        this.refreshPorts();
    };

    firmwareModal = () => {
        return (
            <Modal
                title={'Update Firmware on port: ' + this.state.port}
                visible={this.state.showFirmwareModal}
                onOk={this.handleFirmwareOk}
                onCancel={this.handleFirmwareCancel}
                okButtonProps={{
                    disabled: this.state.firmwareModalFile == '' || this.state.port == '',
                    loading: this.state.isProgramming
                }}
            >
                <div style={{ margin: '20px 20px 20px', textAlign: 'center' }}>
                    <Input
                        addonAfter={
                            <Upload
                                showUploadList={false}
                                beforeUpload={(file: any) => {
                                    this.setState({
                                        firmwareModalFile: file.path,
                                        firmwareModalEnableOk: true
                                    });
                                    return false;
                                }}
                            >
                                <Button size="small" type="link">
                                    <UploadOutlined />
                                </Button>
                            </Upload>
                        }
                        value={this.state.firmwareModalFile}
                        defaultValue={this.state.firmwareModalFile}
                    />
                </div>
            </Modal>
        );
    };
    openSoonTM = () => {
        notification.open({
            message: 'Coming Soon™',
            description: 'I have not yet implemented this functionality, but its coming soon™.',
            onClick: () => {
                console.log('Notification Clicked!');
            }
        });
    };
    showLoadModal = () => {
        this.setState({ showLoadModal: true });
    };
    onLoadOk = () => {
        this.readConfig();
    };
    onLoadCancel = () => {
        this.setState({ showLoadModal: false });
    };
    loadModal = () => {
        return (
            <Modal
                title="Overwrite current config with values from controller?"
                visible={this.state.showLoadModal}
                onOk={this.onLoadOk}
                onCancel={this.onLoadCancel}
                okButtonProps={{ loading: this.state.isReadingConfig }}
            >
                <div style={{ margin: '20px 20px 20px', textAlign: 'center' }}>
                    You will lose any unsaved values. Are you sure you want to proceed?
                </div>
            </Modal>
        );
    };
    render() {
        let prof = this.getCurrentProfileInfo();
        return (
            <div>
                {this.firmwareModal()}
                {this.loadModal()}
                <Layout style={{ height: '1000vw' }}>
                    <Header
                        style={{
                            display: 'flex',
                            justifyContent: 'space-between',
                            flexShrink: 1
                        }}
                        className="header"
                    >
                        <div style={{ display: 'flex', flexShrink: 1, flexBasis: 'auto' }}>
                            LEDSpy Designer ({version})
                        </div>
                        <div
                            style={{
                                flexShrink: 1,
                                flexDirection: 'row',
                                alignItems: 'space-around'
                            }}
                        >
                            Serial Port:{' '}
                            <Select
                                defaultValue={this.state.port}
                                value={this.state.port}
                                style={{ width: 240 }}
                                onChange={this.setPort}
                                size="small"
                                onDropdownVisibleChange={this.onDropdownVisibleChange}
                                loading={this.state.isFetchingPorts}
                            >
                                {this.getPorts().map(item => (
                                    <Option value={item.value} key={item.value}>
                                        {item.label}
                                    </Option>
                                ))}
                            </Select>
                            <Button
                                onClick={this.showFirmwareModal}
                                disabled={this.state.port.length == 0}
                                size="small"
                            >
                                Flash Firmware
                            </Button>
                            <Button
                                onClick={this.writeConfig}
                                size="small"
                                loading={this.state.isProgramming && !this.state.showFirmwareModal}
                                disabled={this.state.port.length == 0}
                            >
                                Upload to Controller
                            </Button>
                            <Button
                                onClick={this.showLoadModal}
                                loading={this.state.isReadingConfig}
                                disabled={this.state.port.length == 0}
                                size="small"
                            >
                                Load From Controller
                            </Button>
                        </div>
                    </Header>
                    <Layout>
                        <Config
                            defaultProfileType={prof.defaultProfileType}
                            breathInterval={prof.breathInterval}
                            onBreathIntervalChange={this.onBreathIntervalChange}
                            onProfileTypeEffectChange={this.setCurrentProfileType}
                            initialConfig={this.getCurrentConfig()}
                            defaultProfile={this.getCurrentProfileInfo().id}
                            onMaxBrightnessChange={this.onMaxBrightnessChange}
                            onBrightnessChange={this.onBrightnessChange}
                            onProfileTypeChange={this.onProfileTypeChange}
                            onProfileChange={this.onProfileChange}
                            onSwitchProfileChange={this.onSwitchProfileChange}
                            defaultMapping={this.state.showMapping}
                            onMappingChange={(mp: boolean) => this.setState({ showMapping: mp })}
                            onConfigChange={this.setCurrentConfig}
                            onSaveConfig={this.saveConfig}
                        />
                        <Content
                            style={{
                                padding: '10vw',
                                margin: 0
                            }}
                        >
                            <Profile
                                isSmashBox={prof.isSmashBox}
                                buttons={prof.buttons}
                                defaultProfileType={prof.defaultProfileType}
                                mapping={this.state.showMapping}
                            />
                        </Content>
                    </Layout>
                </Layout>
            </div>
        );
    }
}

export default Programmer;
