import { hot } from 'react-hot-loader/root';
import * as React from 'react';

import Programmer from './Programmer';

const Application = () => (
  <div>
    <Programmer />
  </div>
);

export default hot(Application);
