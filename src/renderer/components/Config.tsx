import * as React from 'react';
const { dialog } = require('electron').remote;
import {
    Row,
    Col,
    InputNumber,
    Layout,
    Slider,
    Menu,
    Switch,
    Select,
    Button,
    Divider,
    Upload,
    message,
    notification,
    Modal
} from 'antd';
import { DeleteOutlined, PlusOutlined } from '@ant-design/icons';
import {
    Config as Cfg,
    Profile,
    ProfileType,
    ProfileTypeSelect,
    BreathInterval,
    readConfigFromFile
} from '../util/profile';

import './Config.scss';
const { Sider } = Layout;
const { SubMenu } = Menu;
const { Option } = Select;
interface Props {
    onBrightnessChange(bright: number): any;
    onMaxBrightnessChange(maxBright: number): any;
    onSwitchProfileChange(prof1: number, prof2: number, prof3: number): any;
    onProfileTypeChange(isSmashBox: boolean): any;
    onProfileChange(profileId: number): any;
    onProfileTypeEffectChange: (ptype: ProfileType) => any;
    onBreathIntervalChange: (interval: number) => any;
    initialConfig: Cfg;
    defaultProfile: number;
    defaultProfileType: ProfileType;
    breathInterval: number;
    onMappingChange(mode: boolean): any;
    defaultMapping: boolean;
    onConfigChange(cfg: Config): any;
    onSaveConfig: (file: string, errored: (err: Error) => any, success: () => any) => any;
}

interface State {
    config: Cfg;
    currentProfile: number;
    profileEffectType: ProfileType;
    breathInterval: number;
    breathEnabled: boolean;

    showDeleteModal: boolean;
    deleteModalProfile: number;

    popConfirmCallback: () => any;
    popConfirmVisible: boolean;
    selected: string;
    mapping: boolean;
    configStoreFile: string;
    enableLoadSaveButtons: boolean;
    loadLoading: boolean;
    saveLoading: boolean;
    showSaveModal: boolean;
    showLoadModal: boolean;
}
class Config extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            config: this.props.initialConfig,
            currentProfile: this.props.defaultProfile,
            showDeleteModal: false,
            deleteModalProfile: -1,
            profileEffectType: this.props.defaultProfileType,
            breathEnabled: this.props.breathInterval >= BreathInterval.Min,
            breathInterval: this.props.breathInterval,
            popConfirmCallback: () => {},
            popConfirmVisible: false,
            selected: this.props.initialConfig.isSmashBox ? 'SmashBox' : 'HitBox',
            configStoreFile: 'config.json',
            enableLoadSaveButtons: true,
            loadLoading: false,
            saveLoading: false,
            showSaveModal: false,
            showLoadModal: false,

            mapping: this.props.defaultMapping
        };
    }
    static getDerivedStateFromProps = (props: Props, state: State) => {
        return {
            ...state,
            config: props.initialConfig,
            breathEnabled: props.breathInterval >= BreathInterval.Min,
            breathInterval: props.breathInterval,
            currentProfile: props.defaultProfile,
            profileEffectType: props.defaultProfileType,
            selected: props.initialConfig.isSmashBox ? 'SmashBox' : 'HitBox',
            mapping: props.defaultMapping
        };
    };
    setMappingMode = (mp: boolean) => {
        this.setState({ mapping: mp });
        this.props.onMappingChange(mp);
    };
    updateProfileTypeEffect = (value: any) => {
        let ptype: ProfileType = value;
        this.setState({ profileEffectType: ptype });
        this.props.onProfileTypeEffectChange(ptype);
    };
    updateBreatheInterval = (value: any) => {
        if (
            value == NaN ||
            value > BreathInterval.Max ||
            (value < BreathInterval.Min && value != 0)
        ) {
            return;
        }
        this.setState({ breathInterval: value });
        this.props.onBreathIntervalChange(value);
    };
    breatheFmt = (value: any) => {
        return `${((value / BreathInterval.Max) * 100).toPrecision(4)}%`;
    };
    updateBreathIntervalVisibility = (visible: boolean) => {
        if (visible == this.state.breathEnabled) {
            return;
        }
        if (visible) {
            var state = { breathEnabled: true };
            if (this.state.breathInterval < BreathInterval.Min) {
                state = { ...state, ...{ breathInterval: BreathInterval.Default } };
            }
            this.props.onBreathIntervalChange(BreathInterval.Default);
            this.setState(state);
        } else {
            this.props.onBreathIntervalChange(0);
            this.setState({ breathEnabled: false });
        }
    };
    updateBrightness = (value: any) => {
        console.log(value);
        if (isNaN(value)) {
            return;
        }
        let cfg = this.state.config;
        cfg.brightness = value;
        this.setState({ config: cfg });
        this.props.onBrightnessChange(value);
    };
    updateMaxBrightness = (value: any) => {
        if (isNaN(value)) {
            return;
        }
        let cfg = this.state.config;
        cfg.maxBrightness = value;
        if (cfg.brightness > value) {
            cfg.brightness = value;
            this.props.onBrightnessChange(value);
        }
        this.setState({ config: cfg });
        this.props.onMaxBrightnessChange(value);
    };
    updateProfileType = (isSb: boolean) => {
        let cfg = this.state.config;
        cfg.isSmashBox = isSb;
        let selected = isSb ? 'SmashBox' : 'HitBox';
        console.log(cfg);
        this.setState({ config: cfg, selected: selected, popConfirmVisible: false });
        this.props.onProfileTypeChange(cfg.isSmashBox);
    };
    setSmashBox = () => {
        this.updateProfileType(true);
    };
    setHitBox = () => {
        this.updateProfileType(false);
    };
    updateCurrentProfile = (value: any) => {
        if (isNaN(value) || value < 0 || value >= this.state.config.profiles.length) {
            return;
        }
        this.setState({ currentProfile: value });
        this.props.onProfileChange(value);
    };
    onProfileAdd = () => {
        let prof = new Profile();
        let cfg = this.state.config;
        cfg.profiles.push(prof);
        let currentProfile = cfg.profiles.length - 1;
        this.setState({ config: cfg, currentProfile: currentProfile });
        this.props.onProfileChange(currentProfile);
    };
    onProfileDel = (value: any) => {
        if (isNaN(value) || value < 0 || value >= this.state.config.profiles.length) {
            return;
        }
        let cfg = this.state.config;
        if (cfg.profiles.length == 1) {
            console.error('cannot delete last profile');
            return;
        }
        let currentProfile = this.state.currentProfile;
        cfg.profiles.splice(value, 1);
        var nextProfile = currentProfile;
        if (currentProfile == value && currentProfile != 0) {
            nextProfile = currentProfile - 1;
        }
        if (cfg.isSmashBox) {
            if (cfg.switchProfile0 == value || cfg.switchProfile0 >= cfg.profiles.length) {
                cfg.switchProfile0 = nextProfile;
                console.log('change damnit0');
            }
            if (cfg.switchProfile1 == value || cfg.switchProfile1 >= cfg.profiles.length) {
                console.log('change damnit1');
                cfg.switchProfile1 = nextProfile;
            }
            if (cfg.switchProfile2 == value || cfg.switchProfile2 >= cfg.profiles.length) {
                console.log('change damnit2');
                cfg.switchProfile2 = nextProfile;
            }
        }
        this.setState({ config: cfg, currentProfile: nextProfile });
        if (currentProfile == value) {
            this.props.onProfileChange(nextProfile);
        }
    };

    brightnessFmt = (value: any) => {
        return `${((value / this.state.config.maxBrightness) * 100).toPrecision(4)}%`;
    };

    formOpts = () => {
        let list = this.state.config.profiles;
        let opts: { value: number; label: string }[] = [];
        list.forEach((_val, id) => {
            opts.push({ value: id, label: '' + (id + 1) });
        });
        return opts;
    };
    showDeleteModal = (value: any) => {
        if (isNaN(value) || value < 0 || value >= this.state.config.profiles.length) {
            return;
        }
        this.setState({ deleteModalProfile: value, showDeleteModal: true });
    };
    dropDownRender = (menu: any) => {
        return (
            <div>
                {menu}
                <Divider style={{ margin: '4px 0' }} />
                <div style={{ display: 'flex', flexWrap: 'nowrap', padding: 8 }}>
                    <a
                        style={{
                            flex: 'none',
                            padding: '8px',
                            display: 'block',
                            cursor: 'pointer'
                        }}
                        onClick={this.onProfileAdd}
                    >
                        <PlusOutlined /> Add Profile
                    </a>
                </div>
            </div>
        );
    };
    handleDeleteOk = (e: any) => {
        console.log(e);
        this.setState({ showDeleteModal: false });
        this.onProfileDel(this.state.deleteModalProfile);
    };

    handleDeleteCancel = (e: any) => {
        console.log(e);
        this.setState({ showDeleteModal: false });
    };
    deleteModal = () => {
        let disableOk = this.state.config.profiles.length == 1;
        return (
            <Modal
                title="Delete Profile"
                visible={this.state.showDeleteModal}
                onOk={this.handleDeleteOk}
                onCancel={this.handleDeleteCancel}
                okButtonProps={{ disabled: disableOk }}
            >
                <div style={{ margin: '20px 20px 20px', textAlign: 'center' }}>
                    Delete Profile: {this.state.deleteModalProfile}?
                </div>
            </Modal>
        );
    };
    getSwitchProfile = (pos: number) => {
        let cfg = this.state.config;
        let profs = [cfg.switchProfile0, cfg.switchProfile1, cfg.switchProfile2];
        if (isNaN(pos) || pos >= profs.length || pos < 0) {
            return;
        }
        return profs[pos];
    };
    setSwitchProfile = (pos: number) => {
        let cfg = this.state.config;
        let profs = [cfg.switchProfile0, cfg.switchProfile1, cfg.switchProfile2];
        if (isNaN(pos) || pos >= profs.length || pos < 0) {
            return () => {};
        }
        return (profileId: number) => {
            profs[pos] = profileId;
            cfg.switchProfile0 = profs[0];
            cfg.switchProfile1 = profs[1];
            cfg.switchProfile2 = profs[2];

            this.setState({ config: cfg });
            this.props.onSwitchProfileChange(profs[0], profs[1], profs[2]);
        };
    };
    renderSwitchProfileSelect = (pos: number) => {
        return (
            <div>
                Position {'' + (pos + 1) + ': '}
                <Select
                    defaultValue={this.getSwitchProfile(pos)}
                    value={this.getSwitchProfile(pos)}
                    onChange={this.setSwitchProfile(pos)}
                >
                    {this.formOpts().map(item => (
                        <Option value={item.value} key={item.value}>
                            {item.value}
                        </Option>
                    ))}
                </Select>
            </div>
        );
    };
    isSmashBox = () => {
        return this.state.config.isSmashBox;
    };
    popConfirm = () => {
        return (
            <Modal
                title="Are you sure you want to switch controller types?"
                visible={this.state.popConfirmVisible}
                onOk={this.state.popConfirmCallback}
                onCancel={() => this.setState({ popConfirmVisible: false })}
            >
                <div style={{ margin: '20px 20px 20px', textAlign: 'center' }}>
                    Switching profile types will reset any changes you have made to the profile
                    config. Do you wish to proceed?
                </div>
            </Modal>
        );
    };
    onSelect = (e: any) => {
        switch (e.key) {
            case 'SmashBox':
                if (this.isSmashBox()) {
                    return;
                }
                this.setState({
                    popConfirmVisible: true,
                    popConfirmCallback: this.setSmashBox
                });
                break;
            case 'HitBox':
                if (!this.isSmashBox()) {
                    return;
                }
                this.setState({
                    popConfirmVisible: true,
                    popConfirmCallback: this.setHitBox
                });
                break;
        }
    };
    setError = (error: Error) => {
        notification['error']({
            message: error.name,
            description: error.message
        });
    };
    onSaveError = (error: Error) => {
        this.setError(error);
        this.setState({ enableLoadSaveButtons: true, saveLoading: false });
    };
    onLoadError = (error: Error) => {
        this.setError(error);
        this.setState({ enableLoadSaveButtons: true, loadLoading: false });
    };
    onLoadSuccess = (cfg: any) => {
        this.setState({
            enableLoadSaveButtons: true,
            loadLoading: false,
            showLoadModal: false
        });
        this.props.onConfigChange(cfg);
        console.log('success');
        message.success('successfully loaded config from file');
    };
    onSaveSuccess = () => {
        this.setState({
            enableLoadSaveButtons: true,
            saveLoading: false,
            showSaveModal: false
        });
        console.log('success');
        message.success('successfully saved config to file');
    };
    showLoadDialog = (file: any) => {
        // TODO have an ok/cancel dialog
        this.setState({
            showLoadModal: true,
            configStoreFile: file.path,
            enableLoadSaveButtons: false
        });
        return false;
    };
    onLoadOk = () => {
        this.setState({ loadLoading: true });
        readConfigFromFile(this.state.configStoreFile, this.onLoadError, this.onLoadSuccess);
    };
    onLoadCancel = () => {
        this.setState({ showLoadModal: false, enableLoadSaveButtons: true });
    };
    showSaveDialog = (_e: any) => {
        dialog.showSaveDialog(
            {
                title: 'Save Controller Configuration',
                defaultPath: this.state.configStoreFile,
                filters: [{ name: 'Configuration Files', extensions: ['json'] }]
            },
            this.onSave
        );
    };
    onSave = (file: string) => {
        if (!file) {
            return;
        }
        console.log(file);
        this.setState({ saveLoading: true, enableLoadSaveButtons: false, configStoreFile: file });
        this.props.onSaveConfig(file, this.onSaveError, this.onSaveSuccess);
    };
    loadModal = () => {
        return (
            <Modal
                title="Overwrite current config with values from file?"
                visible={this.state.showLoadModal}
                onOk={this.onLoadOk}
                onCancel={this.onLoadCancel}
                okButtonProps={{ loading: this.state.loadLoading }}
            >
                <div style={{ margin: '20px 20px 20px', textAlign: 'center' }}>
                    You will lose any unsaved values. Are you sure you want to proceed?
                </div>
            </Modal>
        );
    };
    render() {
        const inputBrightness = this.state.config.brightness;
        const inputMaxBrightness = this.state.config.maxBrightness;
        let disableDelete = this.state.config.profiles.length == 1;
        const inputInterval = this.state.breathInterval;
        return (
            <div>
                {this.deleteModal()}
                {this.loadModal()}
                {this.popConfirm()}
                <Sider width={'28vw'} style={{ height: '100%', borderRight: 0 }}>
                    <div className="logo" />
                    <Menu
                        theme="dark"
                        defaultSelectedKeys={[this.isSmashBox() ? 'SmashBox' : 'HitBox']}
                        selectedKeys={[this.state.selected]}
                        mode="inline"
                        onSelect={this.onSelect}
                    >
                        <SubMenu key="sub1" title="Controller Type">
                            <Menu.Item key="SmashBox">Smash Box</Menu.Item>
                            <Menu.Item key="HitBox">Hit Box</Menu.Item>
                        </SubMenu>
                        <SubMenu key="sub2" title="Effects">
                            <Divider>Profile Effect Type</Divider>
                            <Select
                                style={{ width: '100%' }}
                                size="small"
                                value={this.state.profileEffectType}
                                onChange={this.updateProfileTypeEffect}
                            >
                                {ProfileTypeSelect.map(item => (
                                    <Option value={item.value} key={item.value}>
                                        {item.label}
                                    </Option>
                                ))}
                            </Select>
                            <Divider>Breathing Effect</Divider>
                            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                                Enable Breathing:
                                <Switch
                                    defaultChecked={this.state.breathEnabled}
                                    style={{ alignItems: 'right' }}
                                    onChange={this.updateBreathIntervalVisibility}
                                />
                            </div>
                            <div
                                style={{
                                    display: this.state.breathEnabled ? 'flex' : 'none'
                                }}
                            >
                                <Slider
                                    style={{
                                        width: '65%'
                                    }}
                                    min={BreathInterval.Min}
                                    max={BreathInterval.Max}
                                    onChange={this.updateBreatheInterval}
                                    tipFormatter={this.breatheFmt}
                                    value={
                                        typeof inputInterval === 'number'
                                            ? inputInterval
                                            : BreathInterval.Min
                                    }
                                    step={0.0001}
                                />
                                <InputNumber
                                    style={{
                                        margin: '0 1vw'
                                    }}
                                    size="small"
                                    min={BreathInterval.Min}
                                    max={BreathInterval.Max}
                                    step={0.0001}
                                    formatter={this.breatheFmt}
                                    value={inputInterval}
                                    onChange={this.updateBreatheInterval}
                                />
                            </div>
                        </SubMenu>
                        <SubMenu key="sub3" title="Brightness">
                            <Divider>Maximum Brightness</Divider>
                            <div style={{ display: 'flex' }}>
                                <Slider
                                    min={0}
                                    style={{
                                        width: '65%'
                                    }}
                                    max={1}
                                    onChange={this.updateMaxBrightness}
                                    value={
                                        typeof inputMaxBrightness === 'number'
                                            ? inputMaxBrightness
                                            : 0
                                    }
                                    step={0.01}
                                />

                                <InputNumber
                                    min={0}
                                    max={1}
                                    style={{
                                        margin: '0 1vw'
                                    }}
                                    step={0.01}
                                    value={inputMaxBrightness}
                                    onChange={this.updateMaxBrightness}
                                />
                            </div>
                            <Divider>Brightness</Divider>
                            <div style={{ display: 'flex' }}>
                                <Slider
                                    min={0}
                                    style={{
                                        width: '65%'
                                    }}
                                    max={this.state.config.maxBrightness}
                                    onChange={this.updateBrightness}
                                    tipFormatter={this.brightnessFmt}
                                    value={
                                        typeof inputBrightness === 'number' ? inputBrightness : 0
                                    }
                                    step={0.01}
                                />
                                <InputNumber
                                    min={0}
                                    max={this.state.config.maxBrightness}
                                    style={{
                                        margin: '0 1vw'
                                    }}
                                    step={0.01}
                                    formatter={this.brightnessFmt}
                                    value={inputBrightness}
                                    onChange={this.updateBrightness}
                                />
                            </div>
                        </SubMenu>
                        <SubMenu key="sub4" title="Profile Settings">
                            Profile:{' '}
                            <Select
                                defaultValue={this.state.currentProfile}
                                value={this.state.currentProfile}
                                onChange={this.updateCurrentProfile}
                                optionLabelProp="label"
                                dropdownMatchSelectWidth={false}
                                dropdownRender={this.dropDownRender}
                            >
                                {this.formOpts().map(item => (
                                    <Option value={item.value} key={item.value}>
                                        <Row justify="space-between">
                                            <Col>{item.value} </Col>
                                            <Col>
                                                <Button
                                                    icon={<DeleteOutlined />}
                                                    size="small"
                                                    danger
                                                    disabled={disableDelete}
                                                    onClick={() => this.showDeleteModal(item.value)}
                                                />
                                            </Col>
                                        </Row>
                                    </Option>
                                ))}
                            </Select>
                            <div style={{ display: this.isSmashBox() ? 'inherit' : 'none' }}>
                                <Divider style={{ display: 'flex', flexShrink: 1 }}>
                                    Smash Box Profile Switch Settings
                                </Divider>
                                {this.renderSwitchProfileSelect(0)}
                                {this.renderSwitchProfileSelect(1)}
                                {this.renderSwitchProfileSelect(2)}
                            </div>
                        </SubMenu>
                        <SubMenu key="sub5" title="Mapping Mode">
                            Enable Mapping Mode:{' '}
                            <Switch
                                checked={this.state.mapping ? true : false}
                                onChange={this.setMappingMode}
                            />
                        </SubMenu>
                        <SubMenu key="sub6" title="Import/Export Configuration">
                            <div>
                                <Button
                                    disabled={!this.state.enableLoadSaveButtons}
                                    loading={this.state.saveLoading}
                                    onClick={this.showSaveDialog}
                                >
                                    Save
                                </Button>
                                <Upload
                                    accept=".json"
                                    showUploadList={false}
                                    beforeUpload={this.showLoadDialog}
                                >
                                    <Button
                                        disabled={!this.state.enableLoadSaveButtons}
                                        loading={this.state.loadLoading}
                                    >
                                        Load
                                    </Button>
                                </Upload>
                            </div>
                        </SubMenu>
                    </Menu>
                </Sider>
            </div>
        );
    }
}

export default Config;
