import * as React from 'react';

import { Popconfirm, Button, Row, InputNumber, Checkbox, Switch } from 'antd';
import { colorDisabled } from './Profile';
import { Mapping, colorToHex } from '../util/profile';
const ColorPicker = require('rc-color-picker');

import './Button.scss';
//TODO figure out the fucking style for this in dark...
interface Props {
    enableReact: boolean;
    defaultColor: string;
    defaultColorReact: string;
    defaultMapping: Mapping;
    isLarge: boolean;
    id: string;
    label: string;
    onChange(color: string, colorReact: string, all: boolean): any;
    onMappingChange(mapping: Mapping): any;
    mapping: boolean;
}

interface State {
    visible: boolean;
    color: string;
    colorTmp: string;
    colorReact: string;
    colorReactTmp: string;
    displayColorPicker: boolean;
    displayReactColorPicker: boolean;
    mappingDisable: boolean;
    mappingValue: number;
    enableReact: boolean;
    applyAll: boolean;
}
class ButtonCfg extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            visible: false,
            color: this.props.defaultColor,
            colorReact: this.props.defaultColorReact,
            displayColorPicker: false,
            displayReactColorPicker: false,
            colorTmp: this.props.defaultColor,
            colorReactTmp: this.props.defaultColorReact,
            mappingDisable: this.props.defaultMapping.disabled,
            mappingValue: this.props.defaultMapping.value,
            enableReact: this.props.enableReact,
            applyAll: false
        };
    }

    showModal = () => {
        this.setState({
            visible: true
        });
    };

    handleOk = (e: any) => {
        console.log(e);
        const state = this.state;
        var applyAll = state.applyAll;
        var newState = {
            visible: false,
            color: state.colorTmp,
            colorReact: state.colorReact,
            applyAll: false
        };
        if (this.props.enableReact) {
            newState.colorReact = state.colorReactTmp;
        }
        this.setState(newState);
        this.props.mapping
            ? this.props.onMappingChange({
                  value: state.mappingValue,
                  disabled: state.mappingDisable
              })
            : this.props.onChange(newState.color, newState.colorReact, applyAll);
    };
    static getDerivedStateFromProps = (props: Props, state: State) => {
        if (state.visible) {
            return {};
        }

        let newState: any = {
            ...state,
            colorReact: props.defaultColorReact,
            colorReactTmp: props.defaultColorReact,
            colorTmp: props.defaultColor,
            color: props.defaultColor,
            enableReact: props.enableReact
        };
        if (props.mapping) {
            const m = props.defaultMapping;
            newState = { ...newState, mappingValue: m.value, mappingDisable: m.disabled };
        }
        return newState;
    };

    handleCancel = (e: any) => {
        console.log(e);
        this.setState({
            visible: false,
            displayColorPicker: false,
            colorTmp: this.state.color,
            colorReactTmp: this.state.colorReact,
            mappingDisable: this.props.defaultMapping.disabled,
            mappingValue: this.props.defaultMapping.value,
            displayReactColorPicker: false
        });
    };

    updateColor = (value: any) => {
        this.setState({ colorTmp: value.color });
    };
    setMappingEnable = (e: any) => {
        const state = this.state;
        const disabled = !e.target.checked;
        this.setState({
            mappingDisable: disabled,
            color: disabled ? colorDisabled : state.color,
            colorTmp: disabled ? colorDisabled : state.colorTmp,
            colorReact: disabled ? colorDisabled : state.colorReact,
            colorReactTmp: disabled ? colorDisabled : state.colorReactTmp
        });
    };
    setMappingValue = (value: any) => {
        if (value == NaN || value > 22 || value < 0) {
            return;
        }
        this.setState({ mappingValue: value });
    };

    updateColorReact = (value: any) => {
        if (this.state.enableReact) {
            this.setState({ colorReactTmp: value.color });
        }
    };

    luma = (color: string | Uint8Array) => {
        var rgb = typeof color === 'string' ? colorToHex(color) : color;
        return 0.2126 * rgb[0] + 0.7152 * rgb[1] + 0.0722 * rgb[2]; // SMPTE C, Rec. 709 weightings
    };
    contrastingColor = (color: string) => {
        return this.luma(color) >= 165 ? '#000' : '#fff';
    };

    buttonStyle = () => {
        let size = '4vw';
        if (this.props.isLarge) {
            size = '5vw';
        }
        let style: any = {
            display: 'flex',
            flexBasis: 'size',
            flexShrink: 1,
            flexGrow: 1,
            backgroundColor: this.state.color,
            height: size,
            flexDirection: 'row',
            flexWrap: 'wrap',
            justifyContent: 'center',
            color: this.contrastingColor(this.state.color),
            width: size,
            borderWidth: 'thin',
            borderColor: '#fff'
        };
        if (this.state.enableReact) {
            style = {
                ...style,
                borderColor: this.state.colorReact,
                fontSize: '1vw',
                borderWidth: 'thick'
            };
        }
        return style;
    };
    renderColorPopConfirm = () => (
        <div style={{ textAlign: 'center' }}>
            <h1>{'Color Settings for Button'}</h1>
            <h1>{this.props.label}</h1>
            <Row
                style={{
                    display: 'flex',
                    justifyContent: 'space-between'
                }}
            >
                Apply to all buttons:
                <Switch
                    checked={this.state.applyAll ? true : false}
                    onChange={(all: boolean) => this.setState({ applyAll: all })}
                />
            </Row>
            <Row
                style={{
                    display: 'flex',
                    justifyContent: 'space-between'
                }}
            >
                Color:
                <ColorPicker
                    enableAlpha={false}
                    color={this.state.colorTmp}
                    onChange={this.updateColor}
                ></ColorPicker>
            </Row>
            {this.state.enableReact ? (
                <Row
                    style={{
                        justifyContent: 'space-between'
                    }}
                >
                    Reactive Color:
                    <ColorPicker
                        align={{ offset: [0, 350] }}
                        enableAlpha={false}
                        color={this.state.colorReactTmp}
                        onChange={this.updateColorReact}
                    ></ColorPicker>
                </Row>
            ) : (
                <div />
            )}
        </div>
    );
    renderMappingPopConfirm = () => (
        <div style={{ textAlign: 'center' }}>
            <h1>{'Button LED Mapping'}</h1>
            <h1>{this.props.label}</h1>

            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                Button Enabled:
                <Checkbox
                    checked={this.state.mappingDisable ? false : true}
                    onChange={this.setMappingEnable}
                />
            </div>
            <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                LED Index:{' '}
                <InputNumber
                    style={{
                        margin: '0 1vw'
                    }}
                    size="small"
                    min={0}
                    max={22}
                    step={1}
                    value={this.state.mappingValue}
                    onChange={this.setMappingValue}
                />
            </div>
        </div>
    );
    renderLabel = (lbl: string) => {
        const l = lbl.split(' ');
        if (l.length == 1) {
            return lbl;
        }
        var ret: string = '';
        l.map((str: string) => (ret += str.charAt(0)));
        return ret;
    };
    render() {
        return (
            <div>
                <Popconfirm
                    placement="top"
                    title={
                        this.props.mapping
                            ? this.renderMappingPopConfirm()
                            : this.renderColorPopConfirm()
                    }
                    onConfirm={this.handleOk}
                    onCancel={this.handleCancel}
                    okText="Yes"
                    cancelText="No"
                >
                    <Button
                        shape="circle"
                        size="large"
                        block={true}
                        style={this.buttonStyle()}
                        onClick={this.showModal}
                    >
                        {this.props.mapping
                            ? this.props.defaultMapping.value
                            : this.renderLabel(this.props.label)}
                    </Button>
                </Popconfirm>
            </div>
        );
    }
}

export default ButtonCfg;
