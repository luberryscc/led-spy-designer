import * as React from 'react';
import { ProfileType, SmashBoxButtonId, get_button_label, Mapping } from '../util/profile';
import ButtonCfg from './Button';
import { Row, Col } from 'antd';

export const colorDisabled = '#303030';

export class ButtonInfo {
    onChange: (color: string, colorReact: string) => any;
    onMappingChange: (mapping: Mapping) => any;
    color: string = '';
    colorReact: string = '';
    id: number = 0;
    mapping: Mapping = new Mapping();
    constructor() {
        this.onChange = (color: string, colorReact: string) => {
            console.log('color update: colors[' + color + ',' + colorReact + ']');
        };
        this.onMappingChange = (mapping: Mapping) => {
            console.log('mapping: ' + mapping);
        };
    }
}

interface Props {
    defaultProfileType: ProfileType;
    isSmashBox: boolean;
    buttons: ButtonInfo[];
    mapping: boolean;
}

interface State {
    isSmashBox: boolean;
    enableReact: boolean;
}
class Profile extends React.Component<Props, State> {
    constructor(props: any) {
        super(props);

        this.state = {
            isSmashBox: this.props.isSmashBox,
            enableReact: this.props.defaultProfileType == ProfileType.Reactive
        };
    }
    renderButton = (btn: ButtonInfo, isLarge: boolean = false) => {
        return (
            <ButtonCfg
                id=""
                isLarge={isLarge}
                enableReact={this.state.enableReact ? true : false}
                defaultColor={btn.color}
                defaultColorReact={btn.colorReact}
                label={get_button_label(this.state.isSmashBox, btn.id)}
                onChange={btn.onChange}
                onMappingChange={btn.onMappingChange}
                defaultMapping={btn.mapping}
                mapping={this.props.mapping}
            />
        );
    };
    static getDerivedStateFromProps = (props: Props, state: State) => {
        return {
            ...state,
            isSmashBox: props.isSmashBox,
            enableReact: props.defaultProfileType == ProfileType.Reactive
        };
    };
    renderSmashBox = () => {
        const kpmoff = '1vw';
        const bid = SmashBoxButtonId;
        //FIXME: eventually i shall fix this horrendous way of laying things out
        //but im not a gui person. maybe i can throw one at the problem
        return (
            <div
                style={{
                    flexWrap: 'nowrap',
                    flexDirection: 'column',
                    flexShrink: 1,
                    flexGrow: 1,
                    flexBasis: 'auto',
                    minWidth: '100vw',
                    width: '100%'
                }}
            >
                <Row>
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.AU])}</Col>
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1.5} />
                    <Col span={1} />
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.ST])}</Col>
                    <Col span={1} />
                    <Col span={1.5} />
                    <Col span={1} />
                    <Col span={1}>
                        <div style={{ marginTop: kpmoff }}>
                            {this.renderButton(this.props.buttons[bid.P1])}
                        </div>
                    </Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.P2])}</Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.P3])}</Col>
                    <Col span={1}>
                        <div style={{ marginTop: kpmoff }}>
                            {this.renderButton(this.props.buttons[bid.P4])}
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col span={1}>
                        <div style={{ marginTop: kpmoff }}>
                            {this.renderButton(this.props.buttons[bid.M1])}
                        </div>
                    </Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.AL])}</Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.AD])}</Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.AR])}</Col>
                    <Col span={1} />
                    <Col span={1.5} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1.5} />
                    <Col span={1} />
                    <Col span={1}>
                        <div style={{ marginTop: kpmoff }}>
                            {this.renderButton(this.props.buttons[bid.K1])}
                        </div>
                    </Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.K2])}</Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.K3])}</Col>
                    <Col span={1}>
                        <div style={{ marginTop: kpmoff }}>
                            {this.renderButton(this.props.buttons[bid.K4])}
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col span={1}>
                        <div style={{ marginTop: '2vw' }}>{'\n'}</div>
                    </Col>
                </Row>
                <Row>
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.M2])}</Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.M3])}</Col>
                    <Col span={1.5} />
                    <Col span={1} />
                    <Col span={1.5} />
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.CU])}</Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.CR])}</Col>
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                </Row>
                <Row>
                    <Col span={1}>
                        <div style={{ marginTop: '1vw' }}>{'\n'}</div>
                    </Col>
                </Row>
                <Row>
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1.5} />
                    <Col style={{ marginLeft: '1vw' }} span={1}>
                        {this.renderButton(this.props.buttons[bid.M4])}
                    </Col>
                    <Col style={{ marginRight: '1vw' }} span={1}>
                        {this.renderButton(this.props.buttons[bid.M5])}
                    </Col>
                    <Col style={{ marginLeft: '1vw' }} span={1}>
                        {this.renderButton(this.props.buttons[bid.CL])}
                    </Col>
                    <Col style={{ marginRight: '1vw' }} span={1}>
                        {this.renderButton(this.props.buttons[bid.CD])}
                    </Col>
                    <Col span={1.5} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                </Row>
                <Row>
                    <Col span={1}>
                        <div style={{ marginTop: '1vw' }}>{'\n'}</div>
                    </Col>
                </Row>
                <Row>
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1.5} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1.5} />
                    <Col style={{ marginLeft: '1vw' }} span={1}>
                        {this.renderButton(this.props.buttons[bid.CM])}
                    </Col>
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                </Row>
            </div>
        );
    };
    renderHitBox = () => {
        const kpmoff = '1vw';
        const bid = SmashBoxButtonId;
        //FIXME: eventually i shall fix this horrendous way of laying things out
        //but im not a gui person. maybe i can throw one at the problem
        return (
            <div
                style={{
                    flexWrap: 'nowrap',
                    flexDirection: 'column',
                    flexShrink: 1,
                    flexGrow: 1,
                    minWidth: '100vw',
                    height: '100%',
                    flexBasis: 'auto',
                    alignSelf: 'center'
                }}
            >
                <Row>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.M2])}</Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.M3])}</Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.M1])}</Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.ST])}</Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.M4])}</Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.M5])}</Col>
                </Row>
                <Row>
                    <Col span={1}>
                        <div style={{ marginTop: kpmoff }}>
                            {this.renderButton(this.props.buttons[bid.AL])}
                        </div>
                    </Col>
                    <Col span={1}>
                        <div style={{ marginTop: kpmoff }}>
                            {this.renderButton(this.props.buttons[bid.AD])}
                        </div>
                    </Col>
                    <Col span={1}>
                        <div style={{ marginTop: '3vw' }}>
                            {this.renderButton(this.props.buttons[bid.AR])}
                        </div>
                    </Col>
                    <Col span={1}>
                        <div style={{ marginTop: kpmoff }}>
                            {this.renderButton(this.props.buttons[bid.P1])}
                        </div>
                    </Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.P2])}</Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.P3])}</Col>
                    <Col span={1}>
                        <div style={{ marginTop: kpmoff }}>
                            {this.renderButton(this.props.buttons[bid.P4])}
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1}>
                        <div style={{ marginTop: kpmoff }}>
                            {this.renderButton(this.props.buttons[bid.K1])}
                        </div>
                    </Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.K2])}</Col>
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.K3])}</Col>
                    <Col span={1}>
                        <div style={{ marginTop: kpmoff }}>
                            {this.renderButton(this.props.buttons[bid.K4])}
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1}>{this.renderButton(this.props.buttons[bid.AU], true)}</Col>
                    <Col span={1} />
                    <Col span={1} />
                    <Col span={1} />
                </Row>
            </div>
        );
    };
    renderProfile(isSmashBox: boolean) {
        console.log(this.props);
        if (isSmashBox) {
            return this.renderSmashBox();
        }

        return this.renderHitBox();
    }
    render() {
        return <div>{this.renderProfile(this.state.isSmashBox)}</div>;
    }
}

export default Profile;
