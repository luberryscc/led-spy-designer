'use strict';
const { getThemeVariables } = require('antd/dist/theme');
const path = require('path');
module.exports = {
  mode: 'development',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    libraryTarget: 'commonjs2'
  },
  node: {
    __dirname: false,
    __filename: false
  },
  resolve: {
    extensions: [
      '.wasm',
      '.ts',
      '.tsx',
      '.mjs',
      '.cjs',
      '.js',
      '.jsx',
      '.json'
    ],
    alias: {
      'rc-trigger': path.resolve(
        __dirname,
        './node_modules/rc-trigger/lib/index.js'
      ) // assuming webpack config file is in the root of the project
    },
    symlinks: false,
    modules: [
      path.join(__dirname, '..', 'app'),
      'node_modules',
      path.resolve(__dirname, './node_modules')
    ]
  },
  module: {
    rules: [
      {
        test: /\.less$/,
        loader: 'less-loader',
        options: {
          lessOptions: {
            modifyVars: getThemeVariables({
              dark: true
            }),
            javascriptEnabled: true
          }
        }
      }
    ]
  },
  resolveLoader: {
    modules: [
      path.join(__dirname, '..', 'app'),
      'node_modules',
      path.resolve(__dirname, './node_modules')
    ]
  },
  devtool: 'source-map',
  externals: {
    serialport: 'commonjs2 serialport'
  },
  plugins: []
};
